CREATE DATABASE QLSV;

CREATE TABLE DMKhoa (
    MaKH varchar(6),
    TenKhoa varchar(30)
);
CREATE TABLE SINHVIEN (
    MaSV varchar(6),
    HoSV varchar(30),
    TenSV varchar(15),
    GioiTinh char(1),
    NgaySinh datetime,
    NoiSinh varchar(50),
    DiaChi varchar(50),
    MaKH varchar(6),
    HocBong int

);

SELECT * FROM `SINHVIEN` 
WHERE MaKH = (
    SELECT MaKH FROM `DMKhoa` 
    WHERE TenKhoa = "Công nghệ thông tin");